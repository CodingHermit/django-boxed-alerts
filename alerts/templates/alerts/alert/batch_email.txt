{% load i18n %}
{% blocktrans %}You have {{ count }} notifications from Inkscape's website.{% endblocktrans %}

{% for alert in alerts %}
  {{ forloop.counter }}. {{ alert.subject }}{% if types > 1 %}[{{alert.alert}}]{% endif %} {% if mode == 'D' %}{{ alert.created|date:"P"}}{% elif mode == "W" %}{{ alert.created|date:"l"}}{% else %}{{ alert.created|date:"jS F"}}{% endif %}{% endfor %}

================================================

{% for alert in alerts %}
{{ forloop.counter }}. {{ alert.subject }} [{{alert.alert}}]
{{ alert.email_body }}
------------------------------------------------
{% endfor %}

{% trans "All my messages" %}: {{ site }}{% url 'alerts' %}

